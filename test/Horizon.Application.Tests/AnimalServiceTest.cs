
using AutoMapper;
using Horizon.Application.Animals;
using Horizon.Application.Contracts.Animals;
using Horizon.Application.Contracts.Mappings;
using Horizon.Application.Contracts.Paging;
using Horizon.Domain.Animals;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Helper;
using Horizon.Domain.Shared.Paging;
using Moq;
using System;
using System.Collections.Generic; 
using System.Threading.Tasks;
using Xunit;

namespace Horizon.Application.Tests
{
    public class AnimalServiceTest
    {
        private readonly Mock<IRepositoryWrapper> _mockRepo;
        private readonly IIdGenerator _idGenerator;
        private readonly IMapper _mapper;
        private readonly IAnimalService _service;

        public AnimalServiceTest()
        {
            _mockRepo = new Mock<IRepositoryWrapper>();

            //create id generator
            _idGenerator = new IdGenerator();

            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingEntitiesProfile());
            });
            _mapper = mockMapper.CreateMapper();

            _service = new AnimalService(_mockRepo.Object, _mapper, _idGenerator);
        }

        #region Create
        [Fact]
        public async void Create_ReturnsSuccess()
        {
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Animals.CreateWithSaveAsync(It.IsAny<Animal>())).Returns(Task.FromResult(1));

            var result = await _service.Create(a);

            //Assert   
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Create_ActionExecutes_ReturnsError()
        {
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Animals.CreateWithSaveAsync(It.IsAny<Animal>())).Returns(Task.FromResult(0));

            var result = await _service.Create(a);

            //Assert   
            Assert.Equal(0, result);
        }
        #endregion 

        #region Update
        [Fact]
        public async void Update_ReturnsSuccess()
        {
            string id = _idGenerator.GenerateId();
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            var mapped = _mapper.Map<Animal>(a);

            _mockRepo.Setup(repo => repo.Animals.FindOneByConditionAsync(p => p.Id == id)).Returns(Task.FromResult(mapped));
            _mockRepo.Setup(repo => repo.Animals.UpdateWithSaveAsync(It.IsAny<Animal>())).Returns(Task.FromResult(1));

            var result = await _service.Update(id, a);

            //Assert   
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Update_ActionExecutes_ReturnsError()
        {
            string id = _idGenerator.GenerateId();
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Animals.UpdateWithSaveAsync(It.IsAny<Animal>())).Returns(Task.FromResult(0));

            var result = await _service.Update(id, a);

            //Assert   
            Assert.Equal(0, result);
        }
        #endregion 

        #region Delete
        [Fact]
        public async void Delete_ReturnsSuccess()
        {
            string id = _idGenerator.GenerateId();
            Animal a = new Animal()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Animals.FindOneByConditionAsync(p => p.Id == id)).Returns(Task.FromResult(a));
            _mockRepo.Setup(repo => repo.Animals.DeleteWithSaveAsync(It.IsAny<Animal>())).Returns(Task.FromResult(1));

            var result = await _service.Delete(id);

            //Assert   
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Delete_ActionExecutes_ReturnsError()
        {
            string id = _idGenerator.GenerateId();

            _mockRepo.Setup(repo => repo.Animals.DeleteWithSaveAsync(It.IsAny<Animal>())).Returns(Task.FromResult(0));

            var result = await _service.Delete(id);

            //Assert   
            Assert.Equal(0, result);
        }
        #endregion 

        #region Get
        [Fact]
        public async void Get_ReturnsSuccess()
        {
            string id = "111";

            Animal a = new Animal()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Animals.FindOneByConditionAsync(p => p.Id == id)).Returns(Task.FromResult(new Animal()
            {
                Id = id,
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            }));

            var result = await _service.Get(id);

            //Assert   
            Assert.NotNull(result);
            Assert.Equal(id, result.Id);
        }

        [Fact]
        public async void Get_ReturnsError()
        {
            string id = "111"; 

            _mockRepo.Setup(repo => repo.Animals.FindOneByConditionAsync(p => p.Id == id)).Returns(Task.FromResult((Animal)null));

            var result = await _service.Get(id);

            //Assert   
            Assert.Null(result); 
        }
        #endregion

        #region GetAll
        [Fact]
        public async void GetAll_ReturnsSuccess()
        {
            PagingDTO paging = new PagingDTO();
            AnimalFilterDTO filter = new AnimalFilterDTO();

            IReadOnlyList<Animal> returnList = new List<Animal>() {
                    new Animal()
                    {
                        Id = "1",
                        LifeNumber = "TR123",
                        Name = "Test Name",
                        Gender = Domain.Shared.Enums.GenderType.Male,
                        BirthDate = DateTime.Now,
                    },
                    new Animal()
                    {
                        Id = "2",
                        LifeNumber = "TR124",
                        Name = "Test Name2",
                        Gender = Domain.Shared.Enums.GenderType.Female,
                        BirthDate = DateTime.Now,
                    }
            };

            _mockRepo.Setup(repo => repo.Animals.FindAllWithPagingAsync(It.IsAny<PagingItem>(), It.IsAny<AnimalFilter>())).Returns(Task.FromResult(returnList));

            var result = await _service.GetAll(paging, filter);

            //Assert    
            Assert.NotNull(result);
            Assert.Equal(returnList.Count, result.Count);
        }

        [Fact]
        public async void GetAll_ReturnsEmpty()
        {
            PagingDTO paging = new PagingDTO();
            AnimalFilterDTO filter = new AnimalFilterDTO();

            IReadOnlyList<Animal> returnList = new List<Animal>() { };

            _mockRepo.Setup(repo => repo.Animals.FindAllWithPagingAsync(It.IsAny<PagingItem>(), It.IsAny<AnimalFilter>())).Returns(Task.FromResult(returnList));

            var result = await _service.GetAll(paging, filter);

            //Assert   
            Assert.NotNull(result);
            Assert.Equal(returnList.Count, result.Count);
        }
        #endregion
    }
}
