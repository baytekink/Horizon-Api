
using AutoMapper;
using Horizon.Application.FeedVisits;
using Horizon.Application.Contracts.FeedVisits;
using Horizon.Application.Contracts.Mappings;
using Horizon.Application.Contracts.Paging;
using Horizon.Domain.FeedVisits;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Helper;
using Horizon.Domain.Shared.Paging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Horizon.Application.Tests
{
    public class FeedVisitTest
    {
        private readonly Mock<IRepositoryWrapper> _mockRepo;
        private readonly IIdGenerator _idGenerator;
        private readonly IMapper _mapper;
        private readonly IFeedVisitService _service;

        public FeedVisitTest()
        {
            _mockRepo = new Mock<IRepositoryWrapper>();

            //create id generator
            _idGenerator = new IdGenerator();

            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingEntitiesProfile());
            });
            _mapper = mockMapper.CreateMapper();

            _service = new FeedVisitService(_mockRepo.Object, _mapper, _idGenerator);
        }

        #region Create
        [Fact]
        public async void Create_ReturnsSuccess()
        {
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.FeedVisits.CreateWithSaveAsync(It.IsAny<FeedVisit>())).Returns(Task.FromResult(1));

            var result = await _service.Create(a);

            //Assert   
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Create_ActionExecutes_ReturnsError()
        {
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.FeedVisits.CreateWithSaveAsync(It.IsAny<FeedVisit>())).Returns(Task.FromResult(0));

            var result = await _service.Create(a);

            //Assert   
            Assert.Equal(0, result);
        }
        #endregion 

        #region Update
        [Fact]
        public async void Update_ReturnsSuccess()
        {
            string id = _idGenerator.GenerateId();
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            var mapped = _mapper.Map<FeedVisit>(a);

            _mockRepo.Setup(repo => repo.FeedVisits.FindOneByConditionAsync(p => p.Id == id)).Returns(Task.FromResult(mapped));
            _mockRepo.Setup(repo => repo.FeedVisits.UpdateWithSaveAsync(It.IsAny<FeedVisit>())).Returns(Task.FromResult(1));

            var result = await _service.Update(id, a);

            //Assert   
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Update_ActionExecutes_ReturnsError()
        {
            string id = _idGenerator.GenerateId();
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.FeedVisits.UpdateWithSaveAsync(It.IsAny<FeedVisit>())).Returns(Task.FromResult(0));

            var result = await _service.Update(id, a);

            //Assert   
            Assert.Equal(0, result);
        }
        #endregion 

        #region Delete
        [Fact]
        public async void Delete_ReturnsSuccess()
        {
            string id = _idGenerator.GenerateId();
            FeedVisit a = new FeedVisit()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.FeedVisits.FindOneByConditionAsync(p => p.Id == id)).Returns(Task.FromResult(a));
            _mockRepo.Setup(repo => repo.FeedVisits.DeleteWithSaveAsync(It.IsAny<FeedVisit>())).Returns(Task.FromResult(1));

            var result = await _service.Delete(id);

            //Assert   
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Delete_ActionExecutes_ReturnsError()
        {
            string id = _idGenerator.GenerateId();

            _mockRepo.Setup(repo => repo.FeedVisits.DeleteWithSaveAsync(It.IsAny<FeedVisit>())).Returns(Task.FromResult(0));

            var result = await _service.Delete(id);

            //Assert   
            Assert.Equal(0, result);
        }
        #endregion 

        #region Get
        [Fact]
        public async void Get_ReturnsSuccess()
        {
            string id = "111";
            string animalId = "TR123";

            FeedVisit a = new FeedVisit()
            {
                AnimalId = animalId,
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.FeedVisits.FindByConditionWithAnimalAsync(p => p.Id == id)).Returns(Task.FromResult(new FeedVisitWithAnimal()
            {
                AnimalId = animalId,
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10,
                Animal = new Domain.Animals.Animal()
                {
                    Id = animalId,
                    BirthDate = DateTime.Now,
                    Gender = Domain.Shared.Enums.GenderType.Female,
                    LifeNumber = "TTrr11",
                    Name = "Test Name"
                }
            }));

            var result = await _service.Get(id);

            //Assert   
            Assert.NotNull(result);
            Assert.Equal(animalId, result.AnimalId);
            Assert.NotNull(result.Animal);
            Assert.Equal(animalId, result.Animal.Id);
        }

        [Fact]
        public async void Get_ReturnsError()
        {
            string id = "111";

            _mockRepo.Setup(repo => repo.FeedVisits.FindOneByConditionAsync(p => p.Id == id)).Returns(Task.FromResult((FeedVisit)null));

            var result = await _service.Get(id);

            //Assert   
            Assert.Null(result);
        }
        #endregion

        #region GetAll
        [Fact]
        public async void GetAll_ReturnsSuccess()
        {
            PagingDTO paging = new PagingDTO();
            FeedingFilterDTO filter = new FeedingFilterDTO();

            IReadOnlyList<FeedVisit> returnList = new List<FeedVisit>() {
                    new FeedVisit()
                    {
                        Id = "1",
                        AnimalId = "TR123",
                        DeviceId = "D111",
                        FeedingDate = DateTime.Now,
                        InTake = 10,
                    },
                    new FeedVisit()
                    {
                        Id = "2",
                        AnimalId = "TR123",
                        DeviceId = "D111",
                        FeedingDate = DateTime.Now,
                        InTake = 10,
                    }
            };

            _mockRepo.Setup(repo => repo.FeedVisits.FindAllWithPagingAsync(It.IsAny<PagingItem>(), It.IsAny<FeedingFilter>())).Returns(Task.FromResult(returnList));

            var result = await _service.GetAll(paging, filter);

            //Assert    
            Assert.NotNull(result);
            Assert.Equal(returnList.Count, result.Count);
        }

        [Fact]
        public async void GetAll_ReturnsEmpty()
        {
            PagingDTO paging = new PagingDTO();
            FeedingFilterDTO filter = new FeedingFilterDTO();

            IReadOnlyList<FeedVisit> returnList = new List<FeedVisit>() { };

            _mockRepo.Setup(repo => repo.FeedVisits.FindAllWithPagingAsync(It.IsAny<PagingItem>(), It.IsAny<FeedingFilter>())).Returns(Task.FromResult(returnList));

            var result = await _service.GetAll(paging, filter);

            //Assert   
            Assert.NotNull(result);
            Assert.Equal(returnList.Count, result.Count);
        }
        #endregion
    }
}
