using System;
using Xunit; 
using Horizon.Api.Controllers;
using Horizon.Application.Contracts.Animals;
using Moq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Horizon.Application.Contracts.Paging;
using System.Collections.Generic;

namespace Horizon.Api.Tests
{
    public class AnimalsControllerTest
    {
        private readonly Mock<IAnimalService> _mockRepo;
        private readonly AnimalsController _controller;

        public AnimalsControllerTest()
        {
            _mockRepo = new Mock<IAnimalService>();
            _controller = new AnimalsController(_mockRepo.Object);
        }

        #region Create
        [Fact]
        public async void Create_ActionExecutes_ReturnsSuccess()
        {
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Create(a)).Returns(Task.FromResult(1));

            var result = await _controller.Create(a);

            //Assert 
            var okObjectResult = result as OkResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);
        }

        [Fact]
        public async void Create_ActionExecutes_ReturnsError()
        {
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Create(a)).Returns(Task.FromResult(0));

            var result = await _controller.Create(a);

            //Assert 
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.NotEqual(200, objectResult.StatusCode);
        }
        #endregion

        #region Update
        [Fact]
        public async void Update_ActionExecutes_ReturnsSuccess()
        {
            string id = "111";
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Update(id, a)).Returns(Task.FromResult(1));

            var result = await _controller.Update(id, a);

            //Assert 
            var okObjectResult = result as OkResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);
        }

        [Fact]
        public async void Update_ActionExecutes_ReturnsError()
        {
            string id = "112";
            AnimalCreateDTO a = new AnimalCreateDTO()
            {
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            };

            _mockRepo.Setup(repo => repo.Update(id, a)).Returns(Task.FromResult(0));

            var result = await _controller.Update(id, a);

            //Assert 
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.NotEqual(200, objectResult.StatusCode);
        }
        #endregion

        #region Delete
        [Fact]
        public async void Delete_ActionExecutes_ReturnsSuccess()
        {
            string id = "111";

            _mockRepo.Setup(repo => repo.Delete(id)).Returns(Task.FromResult(1));

            var result = await _controller.Delete(id);

            //Assert 
            var okObjectResult = result as OkResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);
        }

        [Fact]
        public async void Delete_ActionExecutes_ReturnsError()
        {
            string id = "112";

            _mockRepo.Setup(repo => repo.Delete(id)).Returns(Task.FromResult(0));

            var result = await _controller.Delete(id);

            //Assert 
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.NotEqual(200, objectResult.StatusCode);
        }
        #endregion

        #region Get
        [Fact]
        public async void Get_ActionExecutes_ReturnsSuccess()
        {
            string id = "111";

            _mockRepo.Setup(repo => repo.Get(id)).Returns(Task.FromResult(new AnimalDTO()
            {
                Id = id,
                LifeNumber = "TR123",
                Name = "Test Name",
                Gender = Domain.Shared.Enums.GenderType.Male,
                BirthDate = DateTime.Now,
            }));

            var result = await _controller.Get(id);

            //Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);

            var obj = okObjectResult.Value as AnimalDTO;
            Assert.NotNull(obj);
            Assert.Equal(id, obj.Id);
        }

        [Fact]
        public async void Get_ActionExecutes_ReturnsError()
        {
            string id = "112";

            _mockRepo.Setup(repo => repo.Get(id)).Returns(Task.FromResult((AnimalDTO)null));

            var result = await _controller.Get(id);

            //Assert 
            var objectResult = result as NotFoundResult;
            Assert.NotNull(objectResult);
            Assert.Equal(404, objectResult.StatusCode);
        }
        #endregion

        #region GetAll
        [Fact]
        public async void GetAll_ActionExecutes_ReturnsSuccess()
        {
            PagingDTO paging = new PagingDTO();
            AnimalFilterDTO filter = new AnimalFilterDTO();

            IReadOnlyList<AnimalDTO> returnList = new List<AnimalDTO>() {
                    new AnimalDTO()
                    {
                        Id = "1",
                        LifeNumber = "TR123",
                        Name = "Test Name",
                        Gender = Domain.Shared.Enums.GenderType.Male,
                        BirthDate = DateTime.Now,
                    },
                    new AnimalDTO()
                    {
                        Id = "2",
                        LifeNumber = "TR124",
                        Name = "Test Name2",
                        Gender = Domain.Shared.Enums.GenderType.Female,
                        BirthDate = DateTime.Now,
                    }
            };

            _mockRepo.Setup(repo => repo.GetAll(paging, filter)).Returns(Task.FromResult(returnList));

            var result = await _controller.GetAll(paging, filter);

            //Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);

            var objList = okObjectResult.Value as IReadOnlyList<AnimalDTO>;
            Assert.NotNull(objList);
            Assert.Equal(returnList.Count, objList.Count);
        }

        [Fact]
        public async void GetAll_ActionExecutes_ReturnsEmpty()
        {
            PagingDTO paging = new PagingDTO();
            AnimalFilterDTO filter = new AnimalFilterDTO();

            IReadOnlyList<AnimalDTO> returnList = new List<AnimalDTO>() { };

            _mockRepo.Setup(repo => repo.GetAll(paging, filter)).Returns(Task.FromResult(returnList));

            var result = await _controller.GetAll(paging, filter);

            //Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);

            var objList = okObjectResult.Value as IReadOnlyList<AnimalDTO>;
            Assert.NotNull(objList);
            Assert.Equal(returnList.Count, objList.Count);
        }
        #endregion
    }
}
