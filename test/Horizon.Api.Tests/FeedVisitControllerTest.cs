using Microsoft.Data.Sqlite;
using System;
using System.Data.Common;
using Xunit; 
using Microsoft.EntityFrameworkCore;
using Horizon.Domain.Shared;
using AutoMapper;
using Horizon.Domain.Shared.Helper;
using Horizon.Api.Controllers;
using Moq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Horizon.Application.Contracts.Paging;
using System.Collections.Generic;
using Horizon.Application.Contracts.FeedVisits;

namespace Horizon.Api.Tests
{
    public class FeedVisitControllerTest
    {
        private readonly Mock<IFeedVisitService> _mockRepo;
        private readonly FeedingController _controller;

        public FeedVisitControllerTest()
        {
            _mockRepo = new Mock<IFeedVisitService>();
            _controller = new FeedingController(_mockRepo.Object);
        }

        #region Create
        [Fact]
        public async void Create_ActionExecutes_ReturnsSuccess()
        {
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.Create(a)).Returns(Task.FromResult(1));

            var result = await _controller.Create(a);

            //Assert 
            var okObjectResult = result as OkResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);
        }

        [Fact]
        public async void Create_ActionExecutes_ReturnsError()
        {
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.Create(a)).Returns(Task.FromResult(0));

            var result = await _controller.Create(a);

            //Assert 
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.NotEqual(200, objectResult.StatusCode);
        }
        #endregion

        #region Update
        [Fact]
        public async void Update_ActionExecutes_ReturnsSuccess()
        {
            string id = "111";
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.Update(id, a)).Returns(Task.FromResult(1));

            var result = await _controller.Update(id, a);

            //Assert 
            var okObjectResult = result as OkResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);
        }

        [Fact]
        public async void Update_ActionExecutes_ReturnsError()
        {
            string id = "112";
            FeedVisitCreateDTO a = new FeedVisitCreateDTO()
            {
                AnimalId = "TR123",
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10
            };

            _mockRepo.Setup(repo => repo.Update(id, a)).Returns(Task.FromResult(0));

            var result = await _controller.Update(id, a);

            //Assert 
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.NotEqual(200, objectResult.StatusCode);
        }
        #endregion

        #region Delete
        [Fact]
        public async void Delete_ActionExecutes_ReturnsSuccess()
        {
            string id = "111";

            _mockRepo.Setup(repo => repo.Delete(id)).Returns(Task.FromResult(1));

            var result = await _controller.Delete(id);

            //Assert 
            var okObjectResult = result as OkResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);
        }

        [Fact]
        public async void Delete_ActionExecutes_ReturnsError()
        {
            string id = "112";

            _mockRepo.Setup(repo => repo.Delete(id)).Returns(Task.FromResult(0));

            var result = await _controller.Delete(id);

            //Assert 
            var objectResult = result as ObjectResult;
            Assert.NotNull(objectResult);
            Assert.NotEqual(200, objectResult.StatusCode);
        }
        #endregion

        #region Get
        [Fact]
        public async void Get_ActionExecutes_ReturnsSuccess()
        {
            string id = "111";
            string animalId = "A111";

            _mockRepo.Setup(repo => repo.Get(id)).Returns(Task.FromResult(new FeedVisitGetDTO()
            {
                AnimalId = animalId,
                DeviceId = "D111",
                FeedingDate = DateTime.Now,
                InTake = 10,
                Animal = new Application.Contracts.Animals.AnimalDTO()
                {
                    Id = animalId,
                    BirthDate = DateTime.Now,
                    Gender = Domain.Shared.Enums.GenderType.Female,
                    Name = "Test Name",
                    LifeNumber = "TT111"
                }
            }));

            var result = await _controller.Get(id);

            //Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);

            var obj = okObjectResult.Value as FeedVisitGetDTO;
            Assert.NotNull(obj);
            Assert.Equal(animalId, obj.AnimalId);
            Assert.NotNull(obj.Animal);
            Assert.Equal(animalId, obj.Animal.Id);
        }

        [Fact]
        public async void Get_ActionExecutes_ReturnsError()
        {
            string id = "112";

            _mockRepo.Setup(repo => repo.Get(id)).Returns(Task.FromResult((FeedVisitGetDTO)null));

            var result = await _controller.Get(id);

            //Assert 
            var objectResult = result as NotFoundResult;
            Assert.NotNull(objectResult);
            Assert.Equal(404, objectResult.StatusCode);
        }
        #endregion

        #region GetAll
        [Fact]
        public async void GetAll_ActionExecutes_ReturnsSuccess()
        {
            PagingDTO paging = new PagingDTO();
            FeedingFilterDTO filter = new FeedingFilterDTO();
            filter.FeedingDateStart = new DateTime(2022, 01, 01);
            filter.FeedingDateEnd = new DateTime(2022, 12, 01);

            IReadOnlyList<FeedVisitDTO> returnList = new List<FeedVisitDTO>() {
                    new FeedVisitDTO()
                    {
                        Id = "1",
                        AnimalId = "TR123",
                        DeviceId = "D111",
                        FeedingDate = DateTime.Now,
                        InTake = 10

                    },
                    new FeedVisitDTO()
                    {
                        Id = "2",
                        AnimalId = "TR124",
                        DeviceId = "D111",
                        FeedingDate = DateTime.Now,
                        InTake = 10
                    }
            };

            _mockRepo.Setup(repo => repo.GetAll(paging, filter)).Returns(Task.FromResult(returnList));

            var result = await _controller.GetAll(paging, filter);

            //Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);

            var objList = okObjectResult.Value as IReadOnlyList<FeedVisitDTO>;
            Assert.NotNull(objList);
            Assert.Equal(returnList.Count, objList.Count);
        }

        [Fact]
        public async void GetAll_ActionExecutes_ReturnsEmpty()
        {
            PagingDTO paging = new PagingDTO();
            FeedingFilterDTO filter = new FeedingFilterDTO();
            filter.FeedingDateStart = new DateTime(2022, 01, 01);
            filter.FeedingDateEnd = new DateTime(2022, 12, 01);

            IReadOnlyList<FeedVisitDTO> returnList = new List<FeedVisitDTO>() { };

            _mockRepo.Setup(repo => repo.GetAll(paging, filter)).Returns(Task.FromResult(returnList));

            var result = await _controller.GetAll(paging, filter);

            //Assert 
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            Assert.Equal(200, okObjectResult.StatusCode);

            var objList = okObjectResult.Value as IReadOnlyList<FeedVisitDTO>;
            Assert.NotNull(objList);
            Assert.Equal(returnList.Count, objList.Count);
        }
        #endregion
    }
}
