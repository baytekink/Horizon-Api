﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using Horizon.Domain.Shared;
using Horizon.DataLayer.Repository.Extensions;
using Horizon.Application.Animals;
using Horizon.Application.Contracts.Animals;
using Horizon.Application.Contracts.FeedVisits;
using Horizon.Application.FeedVisits;

namespace Horizon.Application.Extensions
{
    public static class ServiceExtension
    {
        public static IServiceCollection ConfigureHorizonServices(this IServiceCollection services)
        {
            //add db repository
            services.ConfigureRepositoryWrapper();

            //add services
            services.AddTransient<IAnimalService, AnimalService>();
            services.AddTransient<IFeedVisitService, FeedVisitService>();

            return services;
        }
    }
}
