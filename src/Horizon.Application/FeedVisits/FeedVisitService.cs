﻿using AutoMapper;
using Horizon.Application.Contracts.FeedVisits;
using Horizon.Application.Contracts.Paging;
using Horizon.Domain.FeedVisits;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Helper;
using Horizon.Domain.Shared.Paging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Horizon.Application.FeedVisits
{
    public class FeedVisitService : ServiceBase, IFeedVisitService
    {
        IIdGenerator idGenerator;
        public FeedVisitService(IRepositoryWrapper repoWrapper, IMapper mapper, IIdGenerator idGenerator) : base(repoWrapper, mapper)
        {
            this.idGenerator = idGenerator;
        }

        public async Task<int> Create(FeedVisitCreateDTO feed)
        {
            var mappedCreatedObj = _mapper.Map<FeedVisit>(feed);
            mappedCreatedObj.Id = idGenerator.GenerateId();//create a new id

            return await _repoWrapper.FeedVisits.CreateWithSaveAsync(mappedCreatedObj);
        }

        public async Task<int> Update(string id, FeedVisitCreateDTO feed)
        {
            var oldItem = await GetItem(id);
            if (oldItem == null)
                return 0;

            var newItemToUpdate = _mapper.Map<FeedVisit>(feed);
            newItemToUpdate.Id = id;

            return await _repoWrapper.FeedVisits.UpdateWithSaveAsync(newItemToUpdate);
        }

        public async Task<int> Delete(string id)
        {
            var oldItem = await GetItem(id);
            if (oldItem == null)
                return 0;

            return await _repoWrapper.FeedVisits .DeleteWithSaveAsync(_mapper.Map<FeedVisit>(oldItem));
        }

        public async Task<FeedVisitGetDTO> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            var oldItem = await _repoWrapper.FeedVisits.FindByConditionWithAnimalAsync(p => p.Id == id);            
            return _mapper.Map<FeedVisitGetDTO>(oldItem);
        }

        public async Task<IReadOnlyList<FeedVisitDTO>> GetAll(PagingDTO paging, FeedingFilterDTO filter)
        {
            var result = await _repoWrapper.FeedVisits.FindAllWithPagingAsync(_mapper.Map<PagingItem>(paging), _mapper.Map<FeedingFilter>(filter));
            return _mapper.Map<IReadOnlyList<FeedVisitDTO>>(result);
        }

        private async Task<FeedVisit> GetItem(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            var result = await _repoWrapper.FeedVisits .FindOneByConditionAsync(p => p.Id == id);
            return result;
        }
    }
}
