﻿using AutoMapper;
using Horizon.Application.Contracts.Animals;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Helper;
using System;

namespace Horizon.Application
{
    public abstract class ServiceBase  
    {
        protected readonly IRepositoryWrapper _repoWrapper;
        protected readonly IMapper _mapper;
        public ServiceBase(IRepositoryWrapper repoWrapper, IMapper mapper)
        {
            this._repoWrapper = repoWrapper;
            this._mapper = mapper;
        }         
    }
}
