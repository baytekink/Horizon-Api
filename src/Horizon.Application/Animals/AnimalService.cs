﻿using AutoMapper;
using Horizon.Application.Contracts.Animals;
using Horizon.Application.Contracts.Paging;
using Horizon.Domain.Animals;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Helper;
using Horizon.Domain.Shared.Paging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Horizon.Application.Animals
{
    public class AnimalService : ServiceBase, IAnimalService
    {
        IIdGenerator idGenerator;
        public AnimalService(IRepositoryWrapper repoWrapper, IMapper mapper, IIdGenerator idGenerator) : base(repoWrapper, mapper)
        {
            this.idGenerator = idGenerator;
        }

        public async Task<int> Create(AnimalCreateDTO animal)
        {
            var mappedCreatedObj = _mapper.Map<Animal>(animal);
            mappedCreatedObj.Id = idGenerator.GenerateId();//create a new id

            return await _repoWrapper.Animals.CreateWithSaveAsync(mappedCreatedObj);            
        }

        public async Task<int> Update(string id, AnimalCreateDTO animal)
        {
            var oldItem = await GetItem(id);
            if (oldItem == null)
                return 0;

            var newItemToUpdate = _mapper.Map<Animal>(animal);
            newItemToUpdate.Id = id;

            return await _repoWrapper.Animals.UpdateWithSaveAsync(newItemToUpdate);            
        }

        public async Task<int> Delete(string id)
        {
            var oldItem = await GetItem(id);
            if (oldItem == null)
                return 0;

            return await _repoWrapper.Animals.DeleteWithSaveAsync(_mapper.Map<Animal>(oldItem));
        }

        public async Task<AnimalDTO> Get(string id)
        {
            var oldItem = await GetItem(id);
            return _mapper.Map<AnimalDTO>(oldItem);
        }

        public async Task<IReadOnlyList<AnimalDTO>> GetAll( PagingDTO paging,  AnimalFilterDTO filter)
        {
            var result = await _repoWrapper.Animals.FindAllWithPagingAsync(_mapper.Map<PagingItem>(paging), _mapper.Map<AnimalFilter>(filter));
            return  _mapper.Map<IReadOnlyList<AnimalDTO>>(result);
        }
         
        private async Task<Animal> GetItem(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            var result = await _repoWrapper.Animals.FindOneByConditionAsync(p => p.Id == id);
            return result;
        }
    }
}
