﻿using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Application.Contracts.Animals
{
    public class AnimalFilterDTO
    {
        public string LifeNumber { get; set; }
        public GenderType? Gender { get; set; }
        public DateTime? BDateStart { get; set; }
        public DateTime? BDateEnd { get; set; }
    }
}
