﻿using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Application.Contracts.Animals
{
    public class AnimalCreateDTO
    {
        [Required]
        [MaxLength(10)]
        public string LifeNumber { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public GenderType Gender { get; set; }

        [Required]        
        public DateTime BirthDate { get; set; }

        [MaxLength(40)]
        public string FatherLifeNumber { get; set; }

        [MaxLength(40)]
        public string MotherLifeNumber { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
    }
}
