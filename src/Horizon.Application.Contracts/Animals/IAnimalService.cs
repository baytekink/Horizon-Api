﻿using Horizon.Application.Contracts.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Application.Contracts.Animals
{
    public interface IAnimalService
    {
        Task<int> Create(AnimalCreateDTO animal);
        Task<int> Update(string id, AnimalCreateDTO animal);

        Task<int> Delete(string id);
        Task<AnimalDTO> Get(string id);
        Task<IReadOnlyList<AnimalDTO>> GetAll(PagingDTO paging, AnimalFilterDTO filter);
    }
}
