﻿using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Application.Contracts.Animals
{
    public class AnimalDTO : AnimalCreateDTO
    {
        [Required]
        public string Id { get; set; }
    }
}
