﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Horizon.Application.Contracts.Animals;

namespace Horizon.Application.Contracts.FeedVisits
{
    public class FeedingFilterDTO
    { 
        public string DeviceId { get; set; }
         
        public string AnimalId { get; set; }

        //dates are mandatory
        [Required]
        public DateTime FeedingDateStart { get; set; }

        [Required]
        public DateTime FeedingDateEnd { get; set; }

    }
}
