﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Horizon.Application.Contracts.Animals;

namespace Horizon.Application.Contracts.FeedVisits
{
    public class FeedVisitGetDTO: FeedVisitCreateDTO
    {    
        public AnimalDTO Animal { get; set; } 
    }
}
