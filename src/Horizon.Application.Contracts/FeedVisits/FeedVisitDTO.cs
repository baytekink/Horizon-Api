﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Horizon.Application.Contracts.Animals;

namespace Horizon.Application.Contracts.FeedVisits
{
    public class FeedVisitDTO : FeedVisitCreateDTO
    {
        [Required]
        public string Id { get; set; }
    }
}
