﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Horizon.Application.Contracts.Animals;

namespace Horizon.Application.Contracts.FeedVisits
{
    public class FeedVisitCreateDTO
    {
        [Required]
        [MaxLength(40)]
        public string DeviceId { get; set; }

        [Required]
        [MaxLength(40)]
        public string AnimalId { get; set; }

        [Required]
        public DateTime FeedingDate { get; set; }

        [Required]
        [Range(1, 100000)]
        public int InTake { get; set; }
    }
}
