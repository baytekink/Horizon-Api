﻿using Horizon.Application.Contracts.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Application.Contracts.FeedVisits
{
    public interface IFeedVisitService
    {
        Task<int> Create(FeedVisitCreateDTO feed);
        Task<int> Update(string id, FeedVisitCreateDTO feed);

        Task<int> Delete(string id);
        Task<FeedVisitGetDTO> Get(string id);
        Task<IReadOnlyList<FeedVisitDTO>> GetAll(PagingDTO paging, FeedingFilterDTO filter);
    }
}
