﻿using AutoMapper;
using Horizon.Application.Contracts.Animals;
using Horizon.Application.Contracts.FeedVisits;
using Horizon.Application.Contracts.Paging;
using Horizon.Domain.Animals;
using Horizon.Domain.FeedVisits;
using Horizon.Domain.Shared.Paging;
using System;

namespace Horizon.Application.Contracts.Mappings
{
    public class MappingEntitiesProfile : Profile
    {
        public MappingEntitiesProfile()
        {
            //animal mappings
            AddMappingsAnimal();
            //feedvisit mappings
            AddMappingFeedVisits();
            
            //paging mapping
            CreateMap<PagingDTO, PagingItem>();

            //map filters
            CreateMap<AnimalFilterDTO, AnimalFilter>();
            CreateMap<FeedingFilterDTO, FeedingFilter>();

            //may refactor if too many mappings here            
        }

        private void AddMappingsAnimal()
        {
            CreateMap<AnimalDTO, Animal>();
            CreateMap<Animal, AnimalDTO>();
            CreateMap<AnimalCreateDTO, Animal>();
        }

        private void AddMappingFeedVisits()
        {
            CreateMap<FeedVisitDTO, FeedVisit>();
            CreateMap<FeedVisit, FeedVisitDTO>();
            CreateMap<FeedVisitCreateDTO, FeedVisit>();
            CreateMap<FeedVisitWithAnimal, FeedVisitGetDTO>();
        }       
    }
}
