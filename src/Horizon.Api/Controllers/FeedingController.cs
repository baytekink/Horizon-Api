﻿using AutoMapper;
using Horizon.Application.Contracts.FeedVisits;
using Horizon.Application.Contracts.Paging;
using Horizon.Domain.FeedVisits;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Helper;
using Horizon.Domain.Shared.Paging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horizon.Api.Controllers
{
    [Route("api/[controller]/visits")]
    public class FeedingController : BaseController
    {
        IFeedVisitService service;
        public FeedingController(IFeedVisitService service)
        {
            this.service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] FeedVisitCreateDTO feed)
        {
            var result = await service.Create(feed);
            return CheckResult(result);
        }

        [HttpPut("id")]
        public async Task<IActionResult> Update(string id, [FromBody] FeedVisitCreateDTO feed)
        {
            var result = await service.Update(id, feed);
            return CheckResult(result);
        }

        [HttpDelete("id")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await service.Delete(id);
            return CheckResult(result);
        }

        [HttpGet("id")]
        public async Task<IActionResult> Get(string id)
        {
            var item = await service.Get(id);
            if (item == null)
                return NotFoundItem();

            return Ok(item);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PagingDTO paging, [FromQuery] FeedingFilterDTO filter)
        {
            var itemList = await service.GetAll(paging, filter);
            return Ok(itemList);
        }
    }
}
