﻿using AutoMapper;
using Horizon.Application.Contracts;
using Horizon.Application.Contracts.Animals;
using Horizon.Domain.Animals;
using Horizon.Domain.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horizon.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
        protected IActionResult CheckResult(int result)
        {
            return result == 0 ? StatusCode(500, "Internal server error") : Ok();
        }

        protected IActionResult NotFoundItem()
        {
            return NotFound();
        }
    }
}
