﻿using AutoMapper;
using Horizon.Application.Contracts.Animals;
using Horizon.Application.Contracts.Paging;
using Horizon.Domain.Animals;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Helper;
using Horizon.Domain.Shared.Paging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horizon.Api.Controllers
{
    public class AnimalsController : BaseController
    {
        IAnimalService service;
        public AnimalsController(IAnimalService service)
        {
            this.service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] AnimalCreateDTO animal)
        {
            var result = await service.Create(animal);
            return CheckResult(result);
        }

        [HttpPut("id")]
        public async Task<IActionResult> Update(string id, [FromBody] AnimalCreateDTO animal)
        {
            var result = await service.Update(id, animal);
            return CheckResult(result);
        }

        [HttpDelete("id")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await service.Delete(id);
            return CheckResult(result);
        }

        [HttpGet("id")]
        public async Task<IActionResult> Get(string id)
        {
            var item = await service.Get(id);
            if (item == null)
                return NotFoundItem();

            return Ok(item);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PagingDTO paging, [FromQuery] AnimalFilterDTO filter)
        {
            var itemList = await service.GetAll(paging, filter);
            return Ok(itemList);
        }
    }
}
