﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.DataLayer.Extensions
{
    public static class RepositoryContextService
    {
        public static IServiceCollection AddDBContext(this IServiceCollection services)
        {
            services.AddDbContext<RepositoryContext>(options =>
            { 
            });

            return services;
        }
    }
}
