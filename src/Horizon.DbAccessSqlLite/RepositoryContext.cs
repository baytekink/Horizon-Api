﻿using Horizon.Domain.Animals;
using Horizon.Domain.FeedVisits;
using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection;

namespace Horizon.DataLayer
{
    /// <summary>
    /// DB Operations Responsible
    /// </summary>
    public class RepositoryContext : DbContext
    {
        public DbSet<Animal> Animals { get; set; }
        public DbSet<FeedVisit> FeedVisits { get; set; }

        public RepositoryContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreatedAsync();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=HorizonApi.db", options =>
            {
                options.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName);
            });
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
            // Find classes that implements IEntityTypeConfiguration<T>
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(RepositoryContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }
    }
}
