﻿using Horizon.Domain.FeedVisits;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.DataLayer.Configurations
{
    internal class FeedVisitConfig : IEntityTypeConfiguration<FeedVisit>
    {
        public void Configure(EntityTypeBuilder<FeedVisit> builder)
        {
            builder.ToTable("FeedVisit"); // Not required, but I like to explicitly state the table name

            //add primary key
            builder.HasKey(e => e.Id);

            //add field requirements
            builder.Property(x => x.DeviceId).IsRequired().HasMaxLength(40);
            builder.Property(x => x.AnimalId).IsRequired().HasMaxLength(40);
            builder.Property(x => x.FeedingDate).IsRequired();
            builder.Property(x => x.InTake).IsRequired();

            //add indexes            
            builder.HasIndex(e => new { e.FeedingDate, e.AnimalId, e.DeviceId }); //given date which animal which device
            builder.HasIndex(e => new { e.FeedingDate, e.DeviceId, e.AnimalId }).IsUnique(); //given date which device which animal
        }
    }
}
