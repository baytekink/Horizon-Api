﻿using Horizon.Domain.Animals;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.DataLayer.Configurations
{ 
    internal class AnimalsConfig : IEntityTypeConfiguration<Animal>
    { 
        public void Configure(EntityTypeBuilder<Animal> builder)
        {
            builder.ToTable("Animals"); // Not required, but I like to explicitly state the table name

            //add primary key
            builder.HasKey(e => e.Id);

            //add field requirements
            builder.Property(x => x.LifeNumber).IsRequired().HasMaxLength(10);
            builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
            
            builder.Property(x => x.FatherLifeNumber).HasMaxLength(40);
            builder.Property(x => x.MotherLifeNumber).HasMaxLength(40);
            builder.Property(x => x.Description).HasMaxLength(500);

            builder.Property(x => x.Gender).IsRequired();
            builder.Property(x => x.BirthDate).IsRequired();             

            //add indexes            
            builder.HasIndex(e => e.LifeNumber).IsUnique(); //get from life number            
            builder.HasIndex(e => e.Gender); //get from gender
            builder.HasIndex(e => e.BirthDate); //get from bdate
        }
    }
}
