﻿using Horizon.Domain.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Horizon.DataLayer.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositoryContext RepositoryContext { get; set; }
        public RepositoryBase(RepositoryContext repositoryContext)
        {
            this.RepositoryContext = repositoryContext;
        }

        public async Task<int> CreateAsync(T entity)
        {
            var r = await this.RepositoryContext.Set<T>().AddAsync(entity);
            return r != null ? 1 : 0;
        }

        public async Task<int> CreateWithSaveAsync(T entity)
        {
            var r = await CreateAsync(entity);
            if (r > 0)
                r = await SaveAsync();

            return r;
        }

        public int Update(T entity)
        {
            var r = this.RepositoryContext.Set<T>().Update(entity);
            return r != null ? 1 : 0;
        }

        public async Task<int> UpdateWithSaveAsync(T entity)
        {
            var r = Update(entity);
            if (r > 0)
                r = await SaveAsync();

            return r;
        }

        public int Delete(T entity)
        {
            var r = this.RepositoryContext.Set<T>().Remove(entity);
            return r != null ? 1 : 0;
        }

        public async Task<int> DeleteWithSaveAsync(T entity)
        {
            var r = Delete(entity);
            if (r > 0)
                r = await SaveAsync();

            return r;
        }

        public async Task<IReadOnlyList<T>> FindAllAsync()
        {
            return await GetAllItemsAsIQuerable().ToListAsync();
        }

        public async Task<T> FindOneByConditionAsync(Expression<Func<T, bool>> expression)
        {
            return await GetAllItemsAsIQuerable().Where(expression).FirstOrDefaultAsync<T>();
        }

        public async Task<IReadOnlyList<T>> FindByConditionAsync(Expression<Func<T, bool>> expression)
        {
            return await GetAllItemsAsIQuerable().Where(expression).ToListAsync<T>();
        }

        protected IQueryable<T> GetAllItemsAsIQuerable()
        {
            return this.RepositoryContext.Set<T>().AsQueryable().AsNoTracking();
        }

        public async Task<int> SaveAsync()
        {
            return await this.RepositoryContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            if (this.RepositoryContext != null)
            {
                this.RepositoryContext.Dispose();
                this.RepositoryContext = null;
            }
        }
    }
}
