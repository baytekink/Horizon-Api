﻿using Horizon.Domain;
using Horizon.Domain.Animals;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Paging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Horizon.DataLayer.Repository.Animals
{
    public class AnimalsRepository : RepositoryBase<Animal>, IAnimalsRepository
    {
        public AnimalsRepository(RepositoryContext repositoryContext)
           : base(repositoryContext)
        {
        }

        public async Task<IReadOnlyList<Animal>> FindAllWithPagingAsync(PagingItem paging, AnimalFilter filters)
        {
            var list = GetAllItemsAsIQuerable();

            if (filters.BDateStart != null)
                list = list.Where(p => p.BirthDate >= filters.BDateStart);

            if (filters.BDateEnd != null)
                list = list.Where(p => p.BirthDate <= filters.BDateEnd);

            if (!string.IsNullOrWhiteSpace(filters.LifeNumber))
                list = list.Where(p => p.LifeNumber == filters.LifeNumber);

            if (filters.Gender != null)
                list = list.Where(p => p.Gender == filters.Gender);

            return await list.OrderBy(p => p.Id)
                .Skip((paging.PageNumber - 1) * paging.PageSize)
                .Take(paging.PageSize)
                .ToListAsync();
        }        
    }
}
