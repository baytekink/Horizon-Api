﻿using Horizon.Domain;
using Horizon.Domain.Animals;
using Horizon.Domain.FeedVisits;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Paging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Horizon.DataLayer.Repository.FeedVisits
{
    public class FeedVisitsRepository : RepositoryBase<FeedVisit>, IFeedVisitsRepository
    {
        public FeedVisitsRepository(RepositoryContext repositoryContext)
           : base(repositoryContext)
        {
        }

        public async Task<IReadOnlyList<FeedVisit>> FindAllWithPagingAsync(PagingItem paging, FeedingFilter filters)
        {
            var list = GetAllItemsAsIQuerable();

            //dates are mandatory
            list = list.Where(p => p.FeedingDate >= filters.FeedingDateStart && p.FeedingDate <= filters.FeedingDateEnd);

            if (!string.IsNullOrWhiteSpace(filters.AnimalId))
                list = list.Where(p => p.AnimalId == filters.AnimalId);

            if (filters.DeviceId != null)
                list = list.Where(p => p.DeviceId == filters.DeviceId);

            return await list.OrderBy(p => p.Id)
                .Skip((paging.PageNumber - 1) * paging.PageSize)
                .Take(paging.PageSize)
                .ToListAsync();
        }

        public async Task<FeedVisitWithAnimal> FindByConditionWithAnimalAsync(Expression<Func<FeedVisit, bool>> expression)
        {
            var animalsIque = base.RepositoryContext.Animals.AsQueryable().AsNoTracking();

            return await GetAllItemsAsIQuerable().Where(expression).Join(animalsIque,
                    outerKey => outerKey.AnimalId,
                    innerKey => innerKey.Id,
                    (feedVisit, animal) => new FeedVisitWithAnimal
                    {
                        AnimalId = feedVisit.AnimalId,
                        DeviceId = feedVisit.DeviceId,
                        FeedingDate = feedVisit.FeedingDate,
                        InTake = feedVisit.InTake,
                        Animal = animal,
                    }).FirstOrDefaultAsync();
        }

    }
}
