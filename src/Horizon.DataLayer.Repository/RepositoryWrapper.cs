﻿using Horizon.DataLayer.Repository.Animals;
using Horizon.DataLayer.Repository.FeedVisits;
using Horizon.Domain.Animals;
using Horizon.Domain.FeedVisits;
using Horizon.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.DataLayer.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IAnimalsRepository _animal;
        private IFeedVisitsRepository feedVisits;
        public IAnimalsRepository Animals
        {
            get
            {
                if (_animal == null)
                    _animal = new AnimalsRepository(_repoContext);

                return _animal;
            }
        }
        public IFeedVisitsRepository FeedVisits
        {
            get
            {
                if (feedVisits == null)
                    feedVisits = new FeedVisitsRepository(_repoContext);

                return feedVisits;
            }
        }

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

        public async Task<int> SaveAsync()
        {
            return await _repoContext.SaveChangesAsync();
        }
    }
}
