﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horizon.Domain.Shared.Helper
{
    public class IdGenerator : IIdGenerator
    {
        public string GenerateId()
        {
            return Guid.NewGuid().ToString();            
        }
    }
}
