﻿using Horizon.Domain.Shared;
using System;

namespace Horizon.Domain.FeedVisits
{
    /// <summary>
    /// Feeding Visit Entity
    /// </summary>
    public class FeedVisitWithAnimal : FeedVisit
    { 
        public Animals.Animal Animal { get; set; }
    }
}
