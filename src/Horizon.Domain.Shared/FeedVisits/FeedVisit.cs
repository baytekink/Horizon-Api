﻿using System;

namespace Horizon.Domain.FeedVisits
{
    public class FeedVisit  
    {
        public string Id { get; set; }

        public string DeviceId { get; set; }

        public string AnimalId { get; set; }

        public DateTime FeedingDate { get; set; }

        public int InTake { get; set; }
    }
}
