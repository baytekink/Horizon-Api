﻿using Horizon.Domain.Shared;
using System;

namespace Horizon.Domain.FeedVisits
{ 
    public class FeedingFilter 
    {  
        public string DeviceId { get; set; }

        public string AnimalId { get; set; }

        //dates are mandatory
        public DateTime FeedingDateStart { get; set; }
        public DateTime FeedingDateEnd { get; set; }
    }
}
