﻿using Horizon.Domain.FeedVisits;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Domain.FeedVisits
{
    public interface IFeedVisitsRepository : IRepositoryBase<FeedVisit>
    {
        Task<IReadOnlyList<FeedVisit>> FindAllWithPagingAsync(PagingItem paging, FeedingFilter filters);
        Task<FeedVisitWithAnimal> FindByConditionWithAnimalAsync(Expression<Func<FeedVisit, bool>> expression);
    }
}
