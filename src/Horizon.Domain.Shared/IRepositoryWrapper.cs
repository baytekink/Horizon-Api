﻿using Horizon.Domain.Animals;
using Horizon.Domain.FeedVisits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Domain.Shared
{
    public interface IRepositoryWrapper
    {
        IAnimalsRepository Animals { get; }
        IFeedVisitsRepository FeedVisits { get; }
        Task<int> SaveAsync();
    }
}
