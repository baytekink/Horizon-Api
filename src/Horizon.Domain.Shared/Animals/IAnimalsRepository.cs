﻿using Horizon.Domain.Animals;
using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.Domain.Animals
{
    public interface IAnimalsRepository: IRepositoryBase<Animal>
    {
        Task<IReadOnlyList<Animal>> FindAllWithPagingAsync(PagingItem paging, AnimalFilter filters);        
    }
}
