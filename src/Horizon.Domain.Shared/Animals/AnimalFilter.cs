﻿using Horizon.Domain.Shared;
using Horizon.Domain.Shared.Enums;
using System;

namespace Horizon.Domain.Animals
{ 
    public class AnimalFilter 
    {   
        public string LifeNumber { get; set; } 
        
        public GenderType? Gender { get; set; }

        public DateTime? BDateStart { get; set; }
        public DateTime? BDateEnd { get; set; }
    }
}
