﻿using Horizon.Domain.Shared.Enums;
using System;

namespace Horizon.Domain.Animals
{ 
    public class Animal 
    { 
        public string Id { get; set; }
         
        public string LifeNumber { get; set; }
         
        public string Name { get; set; }
        
        public GenderType Gender { get; set; }
         
        public DateTime BirthDate { get; set; }
         
        public string FatherLifeNumber { get; set; }
         
        public string MotherLifeNumber { get; set; }
        
        public string Description { get; set; }        

    }
}
